package client

import data.Message
import data.MessageType
import data.R
import org.glassfish.tyrus.client.ClientManager
import java.net.URI
import java.time.ZonedDateTime

fun main(args: Array<String>) {
    val serverUrl = "ws://${R.IP}:${R.PORT}/ws/chat"

    val client = ClientManager.createClient()

    var name: String?
    do {
        print("Write your name: ")
        name = readLine() ?: return
    } while (null == name || name.isEmpty())

    val session = client.connectToServer(ClientEndpoint(), URI("$serverUrl/$name/"))
    session.basicRemote.sendObject(Message(MessageType.LOGIN, name, name, ZonedDateTime.now().shortTime()))

    println("You enter in the chat")

    while (true) {
        val message = readLine() ?: ""
        val type = getMessageType(message)

        session.basicRemote.sendObject(Message(type, message, name, ZonedDateTime.now().shortTime()))

        if (type == MessageType.LOGOUT) {
            break
        }
    }
}

fun getMessageType(msg: String): MessageType {
    return when (msg) {
        "/quit" -> MessageType.LOGOUT
        "/users" -> MessageType.USER_LIST
        else -> MessageType.MESSAGE
    }
}

private fun ZonedDateTime.shortTime(): String {
    return "[${this.hour}:${this.minute}:${this.second}]"
}