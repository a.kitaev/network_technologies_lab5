package client

import data.Message
import data.decoders.MessageDecoder
import data.encoders.MessageEncoder
import javax.websocket.ClientEndpoint
import javax.websocket.OnMessage

@ClientEndpoint(
    encoders = [MessageEncoder::class],
    decoders = [MessageDecoder::class]
)
class ClientEndpoint {

    @OnMessage
    fun onMessage(message: Message) {
        println("${message.received} ${message.sender}: ${message.content}")
    }
}