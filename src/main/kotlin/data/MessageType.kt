package data

enum class MessageType {
    LOGIN,
    MESSAGE,
    LOGOUT,
    USER_LIST
}