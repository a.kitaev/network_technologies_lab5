package data.decoders

import com.google.gson.Gson
import data.Message
import java.io.StringReader
import javax.websocket.Decoder
import javax.websocket.EndpointConfig

class MessageDecoder : Decoder.Text<Message> {

    override fun willDecode(s: String?): Boolean {
        return true
    }

    override fun destroy() {
    }

    override fun init(config: EndpointConfig?) {
    }

    override fun decode(jsonString: String?): Message {
        val reader = Gson().newJsonReader(StringReader(jsonString))
        return Gson().fromJson(reader, Message().javaClass)
    }
}