package data

import java.time.ZonedDateTime

data class Message(
    val type: MessageType = MessageType.MESSAGE,
    val content: String = "",
    val sender: String = "",
    val received: String = ZonedDateTime.now().shortTime()
)

private fun ZonedDateTime.shortTime(): String {
    return "[${this.hour}:${this.minute}:${this.second}]"
}