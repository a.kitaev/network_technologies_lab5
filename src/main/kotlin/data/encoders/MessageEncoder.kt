package data.encoders

import com.google.gson.Gson
import data.Message
import javax.websocket.Encoder
import javax.websocket.EndpointConfig

class MessageEncoder : Encoder.Text<Message> {

    override fun destroy() {
    }

    override fun init(config: EndpointConfig?) {
    }

    override fun encode(message: Message): String? {
        return Gson().toJson(message)
    }
}