package server

import data.R
import org.glassfish.tyrus.server.Server

fun main(args: Array<String>) {
    val server = Server(
        R.IP,
        R.PORT,
        "/ws",
        null,
        ServerEndpoint::class.java
    )

    try {
        server.start()
        println("Press any key to stop the server..")
        readLine()
    } catch (e: Exception) {
        e.localizedMessage
    } finally {
        server.stop()
    }
}