package server

import data.Message
import data.MessageType
import data.decoders.MessageDecoder
import data.encoders.MessageEncoder
import java.time.ZonedDateTime
import java.util.*
import javax.websocket.OnClose
import javax.websocket.OnMessage
import javax.websocket.OnOpen
import javax.websocket.Session
import javax.websocket.server.ServerEndpoint

@ServerEndpoint(
    value = "/chat/{user}/",
    encoders = [MessageEncoder::class],
    decoders = [MessageDecoder::class]
)
class ServerEndpoint {

    companion object {
        val peers = Collections.synchronizedSet(HashSet<Session>())
        val clients = mutableMapOf<String, String>()
    }

    @OnOpen
    fun onOpen(session: Session) {
        println("${session.id} joined the chat room.")
        peers.add(session)
    }

    @OnMessage
    fun onMessage(message: Message, session: Session) {
        val answer: Message

        when (message.type) {
            MessageType.LOGIN -> {
                clients[session.id] = message.sender
                answer = Message(
                    MessageType.LOGIN,
                    "${message.sender} entered in the chat",
                    "Server",
                    message.received
                )
            }

            MessageType.LOGOUT -> {
                clients.remove(session.id)
                answer = Message(
                    MessageType.LOGOUT,
                    "${message.sender} leaved chat",
                    "Server",
                    message.received
                )
            }

            MessageType.USER_LIST -> {
                session.basicRemote.sendObject(
                    Message(
                        MessageType.USER_LIST,
                        clients.values.toString(),
                        "Server",
                        ZonedDateTime.now().shortTime()
                    )
                )
                return
            }

            else -> answer = message
        }

        peers.forEach {
            if (it.id != session.id) {
                it.basicRemote.sendObject(answer)
            }
        }
    }

    @OnClose
    fun onClose(session: Session) {
        println("${session.id} left the chat room.")
        peers.remove(session)
    }

    private fun ZonedDateTime.shortTime(): String {
        return "[${this.hour}:${this.minute}:${this.second}]"
    }
}